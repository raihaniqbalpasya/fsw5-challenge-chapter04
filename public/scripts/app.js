class App {
  constructor() {
    this.tipedriver = document.querySelector("#tipedriver");
    this.date = document.querySelector("#date");
    this.waktu = document.querySelector("#waktu");
    this.penumpang = document.querySelector("#penumpang");
    this.loadButton = document.querySelector("#btn-cari");
    this.carContainerElement = document.querySelector("#cars-container");
  }

  async init() {
    if (this.loadButton !== null) {
      this.loadButton.onclick = this.click;
    }
  }

  async loadFilter(filter) {
    const cars = await Main.loadCarsFilter(filter);
    Car.init(cars);
  }

  click = async () => {
    let type = this.tipedriver.options[this.tipedriver.selectedIndex].value;
    let passenger = this.penumpang.value;
    let date = this.date.value;
    let time = this.waktu.value;
    if (type.length !== 0 || date.length !== 0 || time.length !== 0) {
      if (passenger.length === 0) {
        passenger = 0;
      }
      date = new Date(this.date.value);
      await this.loadFilter({ type, passenger, date, time });
    }
    this.cardRender();
  };

  cardRender() {
    let card = "";
    if (Car.list.length !== 0) {
      Car.list.forEach((car) => {
        card += car.render();
      });
      this.carContainerElement.innerHTML = card;
    } else {
      card = `
              <div class="alert alert-danger alert-dismissible fade show" role="alert">
                <strong>Mobil Tidak Ditemukan!</strong> Silahkan input kembali sesuai dengan data mobil yang tersedia
                <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
              </div>
            `;
      this.carContainerElement.innerHTML = card;
    }
  }

  navToggler() {
    const body = document.querySelector(".blocking");
    body.style.display = "block";

    body.addEventListener("click", function () {
      this.style.display = "none";
    });
    const closeBtn = document.querySelector(".close");
    closeBtn.addEventListener("click", function () {
      body.style.display = "none";
    });
  }
}
