class Car extends Component{
  static list = [];

  static init(cars) {
    this.list = cars.map((car) => new this(car));
  }

  constructor(props) {
    super(props);
    let {
    id,
    plate,
    manufacture,
    model,
    image,
    rentPerDay,
    capacity,
    description,
    transmission,
    available,
    type,
    year,
    options,
    specs,
    availableAt,
    } = props;
  
    this.id = id;
    this.plate = plate;
    this.manufacture = manufacture;
    this.model = model;
    this.image = image;
    this.rentPerDay = rentPerDay;
    this.capacity = capacity;
    this.description = description;
    this.transmission = transmission;
    this.available = available;
    this.type = type;
    this.year = year;
    this.options = options;
    this.specs = specs;
    this.availableAt = availableAt;
  }

  render() {
    return `
              <div class="col-lg-4 col-md-6 col-sm-12 mb-4 card py-4 px-4">
                <div>
                      <img class="img-fluid mb-3" src="${this.image}">
                      <div>
                          <h5>${this.manufacture}/${this.model}</h5>
                          <h4 class="fw-bold">Rp ${this.rentPerDay} / hari</h4>
                          <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                      </div>
                      <div>
                          <div class="d-flex justify-content-start">
                              <i class="material-icons">people</i> &nbsp;
                              <p class="align-self-center mb-0">${this.capacity} orang</p>
                          </div>
                          <div class="d-flex justify-content-start mt-2">
                              <i class="material-icons">settings</i> &nbsp;
                              <p class="align-self-center mb-0">${this.transmission}</p>
                          </div>
                          <div class="d-flex justify-content-start mt-2">
                              <i class="material-icons">calendar_today</i> &nbsp;
                              <p class="align-self-center">Tahun ${this.year}</p>
                          </div>
                      </div>
                      <div class="d-grid mx-auto gap-2">
                          <button class="btn btn-success btn_style p-2 w-100">Pilih Mobil</button>
                      </div>
                </div>
              </div>
            `;
        }
}
