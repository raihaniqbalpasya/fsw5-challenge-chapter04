const prevIcon = '<div class="prev-slide"><img class="prev" src="gambar/btn-left.png" style="width:20px; height:20px;" alt="right"></div>';
const nextIcon = '<div class="next-slide"><img class="next" src="gambar/btn-right.png" style="width:20px; height:20px;" alt="left"></div>';

$('.owl-carousel').owlCarousel({
    loop: true,
    margin: 15,
    nav: true,
    navText: [
        prevIcon,
        nextIcon
    ],
    responsive: {
        0: {
            items: 1
        },
        500: {
            items: 1
        },
        800: {
            items: 2
        },
        1200: {
            items: 2.3
        }
    }
});

