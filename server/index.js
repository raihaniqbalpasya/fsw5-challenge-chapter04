const http = require('http');
const fs = require('fs');
const path = require('path');

const hostname = "localhost";
const port = 3000;

const server = http.createServer((req, res)=>{
  if(req.url.match(/.css$/)){
    let cssPath = path.join(__dirname, "../public", req.url);
    let cssReadStream = fs.createReadStream(cssPath, "utf-8");

    res.statusCode = 200;
    res.setHeader("Content-Type", "text/css");
    
    cssReadStream.pipe(res);
  }

  if(req.url.match(/.js$/)){
    let jsPath = path.join(__dirname, "../public", req.url);
    let jsReadStream = fs.createReadStream(jsPath, "utf-8");
    
    res.statusCode = 200;
    res.setHeader("Content-Type", "application/js");
    
    jsReadStream.pipe(res);
  }

  if(req.url.match(/.jpg$/)){
    let jpgPath = path.join(__dirname, "../public", req.url);
    let jpgReadStream = fs.createReadStream(jpgPath);

    res.statusCode = 200;
    res.setHeader("Content-Type", "image/jpg");

    jpgReadStream.pipe(res);
  }

  if(req.url.match(/.png$/)){
    let pngPath = path.join(__dirname, "../public", req.url);
    let pngReadStream = fs.createReadStream(pngPath);

    res.statusCode = 200;
    res.setHeader("Content-Type", "image/png");

    pngReadStream.pipe(res);
  }

  if(req.url === "/"){
    fs.readFile("../public/landing_page.html", "utf-8", (err, html)=>{
      res.statusCode = 200;
      res.setHeader("Content-Type", "text/html");
      res.end(html);
    });
  } else if(req.url === "/cars"){
    fs.readFile("../public/cari_mobil.html", "utf-8", (err, html)=>{
      res.statusCode = 200;
      res.setHeader("Content-Type", "text/html");
      res.end(html);
    });
  }
});

server.listen(port, hostname, ()=>{
  console.log("Server berjalan pada http://localhost:3000");
});